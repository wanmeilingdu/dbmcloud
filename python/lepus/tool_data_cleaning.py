#!/usr/bin/env python
#coding:utf-8
import os
import sys
import string
import time
import datetime
import MySQLdb
path='./include'
sys.path.insert(0,path)
import functions as func


def main():
    try:
        now = datetime.datetime.now()
	before_time = now + datetime.timedelta(minutes = -10)
	#print str(before_time)
        func.mysql_exec("delete from mysql_status where create_time < '%s'" %(before_time),'')
        func.mysql_exec("delete from oracle_status where create_time < '%s'" %(before_time),'')
        func.mysql_exec("delete from oracle_tablespace where create_time < '%s'" %(before_time),'')
    
    except Exception, e:
        print e
        sys.exit(1)

    finally:
        sys.exit(1)


if __name__=='__main__':
    main()
