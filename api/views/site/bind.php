<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \api\models\SignupForm */

$this->title = '绑定账号';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <p>只有投资客户才可以进行绑定账户和查询资产信息。请输入您用于投资的手机号码和身份证号进行绑定。</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'nickname')->textInput(['readonly' => true]) ?>
                <?= $form->field($model, 'mobile') ?>
                <?= $form->field($model, 'identity_card') ?>
                <?= $form->field($model, 'openid')->hiddenInput()->label(false); ?>

                <div class="form-group">
                    <?= Html::submitButton('提交绑定', ['class' => 'btn btn-success btn-large', 'name' => 'signup-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
