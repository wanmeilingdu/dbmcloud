<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "crm_user".
 *
 * @property integer $id
 * @property string $realname
 * @property string $mobile
 * @property string $password
 * @property integer $created_at
 * @property integer $updated_at
 */
class CrmUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crm_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['realname', 'mobile', 'password'], 'required'],
            [['realname','mobile'],'unique'],
            [['created_at', 'updated_at'], 'integer'],
            [['mobile'], 'string', 'min'=>11,'max' => 11],
            [['realname', 'mobile', 'password'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile' => '手机号码',
            'password' => '登录密码',
            'realname' => '真实姓名',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
