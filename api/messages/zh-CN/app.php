<?php
/**
 * Created by PhpStorm.
 * User: funson
 * Date: 2014/10/25
 * Time: 10:33
 */

return [
    'Already Bind' => '您的微信已经进行了绑定操作! 请勿重复绑定。',
    'No Customer Found' => '没有查询到与您身份相匹配的投资信息，无法完成绑定操作。',
];