<?php
namespace api\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use backend\models\WechatHtml;
use backend\models\WechatHtmlSearch;


/**
 * Site controller
 */
class WechatController extends Controller
{

  	//public $request;
	public $_title='';
	
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
		
    }
	
	public function actionHtml($key)
	{

		$html_content = WechatHtml::find()->where(['key'=>"$key",'status'=>1])->one();
		if(!$key || !$html_content){
			throw new HttpException(404, 'No Page Find');	
		}

		$this->layout='@app/views/layouts/html.php';
		return $this->render('html', ['html_content' => $html_content]);

	}

	
	
}
