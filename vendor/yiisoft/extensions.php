<?php

$vendorDir = dirname(__DIR__);

return array (
  '2amigos/yii2-dosamigos-asset-bundle' => 
  array (
    'name' => '2amigos/yii2-dosamigos-asset-bundle',
    'version' => '0.1.0.0',
    'alias' => 
    array (
      '@dosamigos/assets' => $vendorDir . '/2amigos/yii2-dosamigos-asset-bundle',
    ),
  ),
  '2amigos/yii2-table-export-widget' => 
  array (
    'name' => '2amigos/yii2-table-export-widget',
    'version' => '0.1.0.0',
    'alias' => 
    array (
      '@dosamigos/tableexport' => $vendorDir . '/2amigos/yii2-table-export-widget',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.7.7.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  '2amigos/yii2-date-picker-widget' => 
  array (
    'name' => '2amigos/yii2-date-picker-widget',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@dosamigos/datepicker' => $vendorDir . '/2amigos/yii2-date-picker-widget/src',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'dektrium/yii2-rbac' => 
  array (
    'name' => 'dektrium/yii2-rbac',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dektrium/rbac' => $vendorDir . '/dektrium/yii2-rbac',
    ),
    'bootstrap' => 'dektrium\\rbac\\Bootstrap',
  ),
  'leandrogehlen/yii2-treegrid' => 
  array (
    'name' => 'leandrogehlen/yii2-treegrid',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@leandrogehlen/treegrid' => $vendorDir . '/leandrogehlen/yii2-treegrid',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2',
    ),
  ),
  'kartik-v/yii2-widget-typeahead' => 
  array (
    'name' => 'kartik-v/yii2-widget-typeahead',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/typeahead' => $vendorDir . '/kartik-v/yii2-widget-typeahead',
    ),
  ),
  '2amigos/yii2-selectize-widget' => 
  array (
    'name' => '2amigos/yii2-selectize-widget',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@dosamigos/selectize' => $vendorDir . '/2amigos/yii2-selectize-widget/src',
    ),
  ),
  'daixianceng/yii2-smser' => 
  array (
    'name' => 'daixianceng/yii2-smser',
    'version' => '1.3.0.0',
    'alias' => 
    array (
      '@daixianceng/smser' => $vendorDir . '/daixianceng/yii2-smser',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.0.7.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid',
    ),
  ),
  'kartik-v/yii2-export' => 
  array (
    'name' => 'kartik-v/yii2-export',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/export' => $vendorDir . '/kartik-v/yii2-export',
    ),
  ),
  'thiagotalma/yii2-jstree' => 
  array (
    'name' => 'thiagotalma/yii2-jstree',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@talma' => $vendorDir . '/thiagotalma/yii2-jstree',
    ),
  ),
  'yii-dream-team/yii2-jstree' => 
  array (
    'name' => 'yii-dream-team/yii2-jstree',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@yiidreamteam/jstree' => $vendorDir . '/yii-dream-team/yii2-jstree/src',
    ),
  ),
  'forecho/yii2-jqtree' => 
  array (
    'name' => 'forecho/yii2-jqtree',
    'version' => '0.1.0.0',
    'alias' => 
    array (
      '@forecho/jqtree' => $vendorDir . '/forecho/yii2-jqtree',
    ),
  ),
  'marekpetras/yii2-calendarview-widget' => 
  array (
    'name' => 'marekpetras/yii2-calendarview-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@marekpetras/calendarview' => '/',
    ),
  ),
  '2amigos/yii2-date-time-picker-widget' => 
  array (
    'name' => '2amigos/yii2-date-time-picker-widget',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@dosamigos/datetimepicker' => $vendorDir . '/2amigos/yii2-date-time-picker-widget/src',
    ),
  ),
  'kartik-v/yii2-widget-datetimepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datetimepicker',
    'version' => '1.4.1.0',
    'alias' => 
    array (
      '@kartik/datetime' => $vendorDir . '/kartik-v/yii2-widget-datetimepicker',
    ),
  ),
  'ollieday/yii2-uuid' => 
  array (
    'name' => 'ollieday/yii2-uuid',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@ollieday/uuid' => $vendorDir . '/ollieday/yii2-uuid/src',
    ),
  ),
  '2amigos/yii2-multi-select-widget' => 
  array (
    'name' => '2amigos/yii2-multi-select-widget',
    'version' => '0.1.1.0',
    'alias' => 
    array (
      '@dosamigos/multiselect' => $vendorDir . '/2amigos/yii2-multi-select-widget',
    ),
  ),
  'kartik-v/yii2-date-range' => 
  array (
    'name' => 'kartik-v/yii2-date-range',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/daterange' => $vendorDir . '/kartik-v/yii2-date-range',
    ),
  ),
  '2amigos/yii2-gallery-widget' => 
  array (
    'name' => '2amigos/yii2-gallery-widget',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@dosamigos/gallery' => $vendorDir . '/2amigos/yii2-gallery-widget/src',
    ),
  ),
  '2amigos/yii2-file-upload-widget' => 
  array (
    'name' => '2amigos/yii2-file-upload-widget',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@dosamigos/fileupload' => $vendorDir . '/2amigos/yii2-file-upload-widget/src',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput',
    ),
  ),
  'himiklab/yii2-colorbox-widget' => 
  array (
    'name' => 'himiklab/yii2-colorbox-widget',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@himiklab/colorbox' => $vendorDir . '/himiklab/yii2-colorbox-widget',
    ),
  ),
  'nemmo/yii2-attachments' => 
  array (
    'name' => 'nemmo/yii2-attachments',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@nemmo/attachments' => $vendorDir . '/nemmo/yii2-attachments',
    ),
  ),
  'kartik-v/yii2-icons' => 
  array (
    'name' => 'kartik-v/yii2-icons',
    'version' => '1.4.1.0',
    'alias' => 
    array (
      '@kartik/icons' => $vendorDir . '/kartik-v/yii2-icons',
    ),
  ),
);
