<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yiidreamteam\\jstree\\' => array($vendorDir . '/yii-dream-team/yii2-jstree/src'),
    'yii\\swiftmailer\\' => array($vendorDir . '/yiisoft/yii2-swiftmailer'),
    'yii\\jui\\' => array($vendorDir . '/yiisoft/yii2-jui'),
    'yii\\imagine\\' => array($vendorDir . '/yiisoft/yii2-imagine'),
    'yii\\gii\\' => array($vendorDir . '/yiisoft/yii2-gii'),
    'yii\\faker\\' => array($vendorDir . '/yiisoft/yii2-faker'),
    'yii\\debug\\' => array($vendorDir . '/yiisoft/yii2-debug'),
    'yii\\composer\\' => array($vendorDir . '/yiisoft/yii2-composer'),
    'yii\\codeception\\' => array($vendorDir . '/yiisoft/yii2-codeception'),
    'yii\\bootstrap\\' => array($vendorDir . '/yiisoft/yii2-bootstrap'),
    'yii\\' => array($vendorDir . '/yiisoft/yii2'),
    'talma\\' => array($vendorDir . '/thiagotalma/yii2-jstree'),
    'ollieday\\uuid\\' => array($vendorDir . '/ollieday/yii2-uuid/src'),
    'nemmo\\attachments\\' => array($vendorDir . '/nemmo/yii2-attachments'),
    'marekpetras\\calendarview\\' => array($vendorDir . '/marekpetras/yii2-calendarview-widget'),
    'leandrogehlen\\treegrid\\' => array($vendorDir . '/leandrogehlen/yii2-treegrid'),
    'kucha\\ueditor\\' => array($vendorDir . '/kucha/ueditor'),
    'kartik\\typeahead\\' => array($vendorDir . '/kartik-v/yii2-widget-typeahead'),
    'kartik\\select2\\' => array($vendorDir . '/kartik-v/yii2-widget-select2'),
    'kartik\\plugins\\fileinput\\' => array($vendorDir . '/kartik-v/bootstrap-fileinput'),
    'kartik\\icons\\' => array($vendorDir . '/kartik-v/yii2-icons'),
    'kartik\\grid\\' => array($vendorDir . '/kartik-v/yii2-grid'),
    'kartik\\file\\' => array($vendorDir . '/kartik-v/yii2-widget-fileinput'),
    'kartik\\export\\' => array($vendorDir . '/kartik-v/yii2-export'),
    'kartik\\datetime\\' => array($vendorDir . '/kartik-v/yii2-widget-datetimepicker'),
    'kartik\\daterange\\' => array($vendorDir . '/kartik-v/yii2-date-range'),
    'kartik\\base\\' => array($vendorDir . '/kartik-v/yii2-krajee-base'),
    'himiklab\\colorbox\\' => array($vendorDir . '/himiklab/yii2-colorbox-widget'),
    'forecho\\jqtree\\' => array($vendorDir . '/forecho/yii2-jqtree'),
    'dosamigos\\tableexport\\' => array($vendorDir . '/2amigos/yii2-table-export-widget'),
    'dosamigos\\selectize\\' => array($vendorDir . '/2amigos/yii2-selectize-widget/src'),
    'dosamigos\\multiselect\\' => array($vendorDir . '/2amigos/yii2-multi-select-widget'),
    'dosamigos\\gallery\\' => array($vendorDir . '/2amigos/yii2-gallery-widget/src'),
    'dosamigos\\fileupload\\' => array($vendorDir . '/2amigos/yii2-file-upload-widget/src'),
    'dosamigos\\datetimepicker\\' => array($vendorDir . '/2amigos/yii2-date-time-picker-widget/src'),
    'dosamigos\\datepicker\\' => array($vendorDir . '/2amigos/yii2-date-picker-widget/src'),
    'dosamigos\\assets\\' => array($vendorDir . '/2amigos/yii2-dosamigos-asset-bundle'),
    'dektrium\\rbac\\' => array($vendorDir . '/dektrium/yii2-rbac'),
    'daixianceng\\smser\\' => array($vendorDir . '/daixianceng/yii2-smser'),
    'cebe\\markdown\\' => array($vendorDir . '/cebe/markdown'),
    'Ramsey\\Uuid\\' => array($vendorDir . '/ramsey/uuid/src'),
    'Fxp\\Composer\\AssetPlugin\\' => array($vendorDir . '/fxp/composer-asset-plugin'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
);
