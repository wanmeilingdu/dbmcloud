<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "settings".
 *
 * @property integer $organization_id
 * @property string $backend_theme
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Settings extends \yii\db\ActiveRecord
{
    const SKIN_BLUE = 'skin-blue';
    const SKIN_BLUE_LIGHT = 'skin-blue-light';
    const SKIN_GREEN = 'skin-green';
    const SKIN_GREEN_LIGHT = 'skin-green-light';
    const SKIN_PURPLE = 'skin-purple';
    const SKIN_PURPLE_LIGHT = 'skin-purple-light';
    const SKIN_RED = 'skin-red';
    const SKIN_RED_LIGHT = 'skin-red-light';
    const SKIN_YELLOW = 'skin-yellow';
    const SKIN_YELLOW_LIGHT = 'skin-yellow-light';
    const SKIN_BLACK = 'skin-black';
    const SKIN_BLACK_LIGHT = 'skin-black-light';
    const SKIN_NONE = 'skin-none';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    public static function getBackendTheme(){
        return [
            self::SKIN_BLUE => '蓝色',
            self::SKIN_BLUE_LIGHT => '蓝色(轻边栏)',
            self::SKIN_GREEN => '绿色',
            self::SKIN_GREEN_LIGHT => '绿色(轻边栏)',
            self::SKIN_PURPLE => '紫色',
            self::SKIN_PURPLE_LIGHT => '紫色(轻边栏)',
            self::SKIN_RED => '红色',
            self::SKIN_RED_LIGHT => '红色(轻边栏)',
            self::SKIN_YELLOW => '黄色',
            self::SKIN_YELLOW_LIGHT => '黄色(轻边栏)',
            self::SKIN_BLACK => '黑白',
            self::SKIN_BLACK_LIGHT => '黑白(轻)',
            self::SKIN_NONE => '无配色(极简)',
        ];
    }


    /**
     * create_time, update_time to now()
     * crate_user_id, update_user_id to current login user id
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            // BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_id'], 'required'],
            [['organization_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['backend_theme'], 'string', 'max' => 50],
            [['organization_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'organization_id' => Yii::t('app', 'Organization ID'),
            'backend_theme' => Yii::t('app', 'Backend Theme'),
            'enable_modules' => Yii::t('app', 'Enable Modules'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Before save.
     * 
     */
    /*public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            // add your code here
            return true;
        }
        else
            return false;
    }*/

    /**
     * After save.
     *
     */
    /*public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add your code here
    }*/



}
