<?php
/**
 * Created by PhpStorm.
 * User: ruzuojun
 * Date: 2015/10/15
 * Time: 12:42
 */

namespace  backend\helpers;

class Tools {


    static function checkUptime($data){

        if($data ==-1){
            return "---";
        }
        else if($data < 60){
            $result =  $data." 秒";
            return $result;
        }
        else if($data>=60 and $data <3600){
            $result = number_format(($data/60))." 分钟";
            return $result;
        }
        else if($data>=3600 and $data <86400){
            $result = number_format(($data/3600))." 小时";
            return $result;
        }
        else if($data>=86400){
            $result=number_format(($data/86400),0)." 天";
            return $result;
        }

    }

    static function checkValue($data){
        return $data;exit;
        if($data==='-1' ){
            return "---";
        }
        else if($data=='master'){
            return "<span class='btn btn-primary btn-xs'>主库</span>";
        }
        else if($data=='slave'){
            return "<span class='btn btn-warning btn-xs'>备库</span>";
        }
        else if($data=='alone'){
            return "<span class='label label-info'>Alone</span>";
        }
        else if($data=='Yes'){
            return "<span class='label label-success'>Run</span>";
        }
        else if($data=='No'){
            return "<span class='label label-important'>No</span>";
        }
        else if($data=='ON'){
            return "<span class='label label-info'>ON</span>";
        }
        else if($data=='OFF'){
            return "<span class='label '>OFF</span>";
        }
        else{
            return $data;
        }

    }

    static function formatBytes($data)
    {
        if($data==-1)
        {
            return '---';
        }
        else if($data>=0 and $data<1024)
        {
            return number_format(($data))."bytes";
        }
        else if($data>=1024 and $data<1048576)
        {
            return number_format(($data/1024))."KB";
        }
        else if($data>=1048576 and $data<1073741824)
        {
            return number_format(($data/1024/1024))."MB";
        }
        else
        {
            return number_format(($data/1024/1024/1024))."GB";
        }
    }

    static function formatKbytes($data)
    {
        if($data==-1)
        {
            return '---';
        }
        else if($data>=0 and $data<1024)
        {
            return number_format(($data))."KB";
        }
        else if($data>=1024 and $data<1048576)
        {
            return number_format(($data/1024))."MB";
        }
        else if($data>=1048576 and $data<1073741824)
        {
            return number_format(($data/1024/1024),1)."GB";
        }
        else
        {
            return number_format(($data/1024/1024/1024),1)."TB";
        }
    }

    static function formatMbytes($data)
    {
        if($data==-1)
        {
            return '---';
        }
        else if($data>0 and $data<1024)
        {
            return number_format(($data))."MB";
        }
        else if($data>=1024 and $data<1048576)
        {
            return number_format(($data/1024),1)."GB";
        }
        else
        {
            return number_format(($data/1024/1024),1)."TB";
        }
    }

}