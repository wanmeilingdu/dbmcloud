<?php

namespace backend\modules\os\models;

use Yii;

/**
 * This is the model class for table "os_status".
 *
 * @property integer $id
 * @property string $ip
 * @property integer $snmp
 * @property string $tags
 * @property string $hostname
 * @property string $kernel
 * @property string $system_date
 * @property string $system_uptime
 * @property integer $process
 * @property string $load_1
 * @property string $load_5
 * @property string $load_15
 * @property integer $cpu_user_time
 * @property integer $cpu_system_time
 * @property integer $cpu_idle_time
 * @property integer $swap_total
 * @property integer $swap_avail
 * @property integer $mem_total
 * @property integer $mem_used
 * @property integer $mem_free
 * @property integer $mem_shared
 * @property integer $mem_buffered
 * @property integer $mem_cached
 * @property string $mem_usage_rate
 * @property string $mem_available
 * @property integer $disk_io_reads_total
 * @property integer $disk_io_writes_total
 * @property string $net_in_bytes_total
 * @property string $net_out_bytes_total
 * @property string $create_time
 */
class OsStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'os_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip'], 'required'],
            [['snmp', 'process', 'cpu_user_time', 'cpu_system_time', 'cpu_idle_time', 'swap_total', 'swap_avail', 'mem_total', 'mem_used', 'mem_free', 'mem_shared', 'mem_buffered', 'mem_cached', 'disk_io_reads_total', 'disk_io_writes_total', 'net_in_bytes_total', 'net_out_bytes_total'], 'integer'],
            [['load_1', 'load_5', 'load_15'], 'number'],
            [['create_time'], 'safe'],
            [['ip', 'kernel', 'system_date', 'system_uptime', 'mem_usage_rate', 'mem_available'], 'string', 'max' => 50],
            [['tags', 'hostname'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => '主机IP',
            'snmp' => 'SNMP',
            'tags' => '标签',
            'hostname' => '主机名',
            'kernel' => '内核',
            'system_date' => '系统时间',
            'system_uptime' => '运行时间',
            'process' => '进程数',
            'load_1' => '1分钟负载',
            'load_5' => '5分钟负载',
            'load_15' => '15分钟负载',
            'cpu_user_time' => '用户Cpu',
            'cpu_system_time' => '系统Cpu',
            'cpu_idle_time' => '空闲Cpu',
            'swap_total' => 'Swap Total',
            'swap_avail' => 'Swap Avail',
            'mem_total' => 'Mem Total',
            'mem_used' => 'Mem Used',
            'mem_free' => 'Mem Free',
            'mem_shared' => 'Mem Shared',
            'mem_buffered' => 'Mem Buffered',
            'mem_cached' => 'Mem Cached',
            'mem_usage_rate' => 'Mem Usage Rate',
            'mem_available' => 'Mem Available',
            'disk_io_reads_total' => 'Disk Io Reads Total',
            'disk_io_writes_total' => 'Disk Io Writes Total',
            'net_in_bytes_total' => 'Net In Bytes Total',
            'net_out_bytes_total' => 'Net Out Bytes Total',
            'create_time' => 'Create Time',
        ];
    }
}
