<?php

namespace backend\modules\os\models;

use Yii;

/**
 * This is the model class for table "os_disk".
 *
 * @property integer $id
 * @property string $ip
 * @property string $tags
 * @property string $mounted
 * @property string $total_size
 * @property string $used_size
 * @property string $avail_size
 * @property string $used_rate
 * @property string $create_time
 */
class OsDisk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'os_disk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip'], 'required'],
            [['total_size', 'used_size', 'avail_size'], 'integer'],
            [['create_time'], 'safe'],
            [['ip', 'mounted'], 'string', 'max' => 50],
            [['tags'], 'string', 'max' => 100],
            [['used_rate'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'IP',
            'tags' => '标签',
            'mounted' => '挂载点',
            'total_size' => '总容量',
            'used_size' => '已用容量',
            'avail_size' => '可用容量',
            'used_rate' => '使用率',
            'create_time' => '创建时间',
        ];
    }
}
