<?php

namespace backend\modules\sys\controllers;

use Yii;
use backend\models\User;
use yii\web\NotFoundHttpException;
use backend\models\ActionLog;
use c006\alerts\Alerts;

class ProfileController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = User::find()->where(['id'=>Yii::$app->user->identity->organization_id,'id'=>Yii::$app->user->identity->id])->one();

        if(Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                ActionLog::log("更新个人信息");
                Alerts::setMessage('单位个人更新成功!');
                Alerts::setAlertType(Alerts::ALERT_SUCCESS);
            }
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }

}
