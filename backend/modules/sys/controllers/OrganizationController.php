<?php

namespace backend\modules\sys\controllers;

use Yii;
use yii\web\Controller;
use backend\controllers\BaseController;
use common\models\Organization;
use yii\web\NotFoundHttpException;
use backend\models\ActionLog;
use c006\alerts\Alerts;

class OrganizationController extends BaseController
{
    public function actionIndex()
    {
        $model = Organization::find()->where(['id'=>Yii::$app->user->identity->organization_id])->one();
        if(Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->system->writeLog(Yii::t('app','Update ').Yii::t('app','Organization'));
                Alerts::setMessage('单位信息更新成功!');
                Alerts::setAlertType(Alerts::ALERT_SUCCESS);
            }
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }

}
