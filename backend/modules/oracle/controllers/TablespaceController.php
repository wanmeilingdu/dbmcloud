<?php

namespace backend\modules\oracle\controllers;

use Yii;
use backend\modules\oracle\models\OracleTablespace;
use backend\modules\oracle\models\OracleTablespaceSearch;
use backend\modules\oracle\models\OracleTablespaceHistory;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TablespaceController implements the CRUD actions for OracleTablespace model.
 */
class TablespaceController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all OracleTablespace models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OracleTablespaceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Finds the OracleTablespace model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OracleTablespace the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($server)
    {
        $server_array = explode('--',$server);
        $host = $server_array[0];
        $port = $server_array[1];
        if (($model = OracleTablespace::find()->where(['host' => $host,'port'=>$port])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    function actionChart(){
        if($post = Yii::$app->request->post()){
            $host = $post['OracleTablespace']['host'];
            $port = $post['OracleTablespace']['port'];
            $time_from = $post['time_from'];
            $time_to = $post['time_to'];
            $start_time = date('YmdHi',strtotime($time_from));
            $end_time = date('YmdHi',strtotime($time_to));
            $time_interval = 0;

        }
        else{
            $server = Yii::$app->getRequest()->get()['server'];
            $server_array = explode('--',$server);
            $host = $server_array[0];
            $port = $server_array[1];

            $time_interval = Yii::$app->getRequest()->get()['time_interval'];
            $end = Yii::$app->getRequest()->get()['end'];
            $start_time = date('YmdHi',(time()-$end-$time_interval));
            $end_time = date('YmdHi',(time()-$end));
            $time_from = date('Y-m-d',time()-3600*24);
            $time_to = date('Y-m-d',time());
        }

        if (($model = OracleTablespace::findOne(['host' => $host,'port'=>$port])) !== null) {
            $chart_data = OracleTablespaceHistory::find()->where((['host' => $host,'port'=>$port]))->andFilterWhere(['>=', 'YmdHi', $start_time])->andFilterWhere(['<=', 'YmdHi', $end_time])->all();
            return $this->render('chart', [
                'chart_data' => $chart_data,
                'model' => $model,
                'time_interval' => $time_interval,
                'time_from'=>$time_from,
                'time_to'=>$time_to,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }
}
