<?php

namespace backend\modules\oracle\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->redirect(['status/index']);
        //return $this->render('index');
    }
}
