<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\oracle\models\OracleTablespace */

$this->title = 'Create Oracle Tablespace';
$this->params['breadcrumbs'][] = ['label' => 'Oracle Tablespaces', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oracle-tablespace-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
