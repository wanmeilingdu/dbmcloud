<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\oracle\models\OracleTablespaceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="oracle-tablespace-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'host') ?>

    <?= $form->field($model, 'port') ?>

    <?= $form->field($model, 'tags') ?>

    <?= $form->field($model, 'tablespace_name') ?>

    <?php // echo $form->field($model, 'total_size') ?>

    <?php // echo $form->field($model, 'used_size') ?>

    <?php // echo $form->field($model, 'avail_size') ?>

    <?php // echo $form->field($model, 'used_rate') ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
