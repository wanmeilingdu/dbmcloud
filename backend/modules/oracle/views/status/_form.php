<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\oracle\models\OracleStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="oracle-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'host')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'port')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tags')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'connect')->textInput() ?>

    <?= $form->field($model, 'instance_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'instance_role')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'instance_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'database_role')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'open_mode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'protection_mode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'host_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'database_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'startup_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uptime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'version')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'archiver')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'session_total')->textInput() ?>

    <?= $form->field($model, 'session_actives')->textInput() ?>

    <?= $form->field($model, 'session_waits')->textInput() ?>

    <?= $form->field($model, 'dg_stats')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dg_delay')->textInput() ?>

    <?= $form->field($model, 'processes')->textInput() ?>

    <?= $form->field($model, 'session_logical_reads_persecond')->textInput() ?>

    <?= $form->field($model, 'physical_reads_persecond')->textInput() ?>

    <?= $form->field($model, 'physical_writes_persecond')->textInput() ?>

    <?= $form->field($model, 'physical_read_io_requests_persecond')->textInput() ?>

    <?= $form->field($model, 'physical_write_io_requests_persecond')->textInput() ?>

    <?= $form->field($model, 'db_block_changes_persecond')->textInput() ?>

    <?= $form->field($model, 'os_cpu_wait_time')->textInput() ?>

    <?= $form->field($model, 'logons_persecond')->textInput() ?>

    <?= $form->field($model, 'logons_current')->textInput() ?>

    <?= $form->field($model, 'opened_cursors_persecond')->textInput() ?>

    <?= $form->field($model, 'opened_cursors_current')->textInput() ?>

    <?= $form->field($model, 'user_commits_persecond')->textInput() ?>

    <?= $form->field($model, 'user_rollbacks_persecond')->textInput() ?>

    <?= $form->field($model, 'user_calls_persecond')->textInput() ?>

    <?= $form->field($model, 'db_block_gets_persecond')->textInput() ?>

    <?= $form->field($model, 'create_time')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
