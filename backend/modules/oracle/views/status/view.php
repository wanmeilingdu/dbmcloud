<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use backend\helpers\Tools;

/* @var $this yii\web\View */
/* @var $model backend\modules\oracle\models\OracleStatus */

$this->title = $model->host . ':' . $model->port . ' [' . $model->tags .':'.$model->database_role. ']';
$this->params['breadcrumbs'][] = ['label' => 'Oracle 性能监控平台', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->host . ':' . $model->port;
?>
<div class="oracle-status-view">

    <?php echo $this->render('_tab', ['model' => $model]); ?>


    <div class="container-fluid mt5">
        <div class="col-xs-8 text-left">
            <table class="table table-bordered table-hover table-striped" style="margin-top: 15px;">
                <thead>
                <tr>
                    <th>指标</th>
                    <th>数值</th>
                    <th>备注</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>实例名称</td>
                    <td><?= $model->host . ':' . $model->port ?></td>
                    <td>当前数据库的连接主机和端口信息</td>
                </tr>
                <tr>
                    <td>采集时间</td>
                    <td><?= $model->create_time ?></td>
                    <td>采集进程最后执行采集任务的时间</td>
                </tr>
                <tr>
                    <td>连接状态</td>
                    <td><span class="btn btn-success btn-xs">正常</span></td>
                    <td>当前数据库是否正常连接</td>
                </tr>
                <tr>
                    <td>运行时间</td>
                    <td><?= Tools::checkUptime(time()-strtotime($model->uptime)); ?></td>
                    <td>当前数据库自启动以来运行时间</td>
                </tr>
                <tr>
                    <td>版本号</td>
                    <td><?= $model->version ?></td>
                    <td>当前数据库运行版本</td>
                </tr>
                <tr>
                    <td>数据库角色</td>
                    <td><?= $model->database_role ?></td>
                    <td>主库或者备库</td>
                </tr>
                <tr>
                    <td>进程池使用率</td>
                    <td><?= intval(($model->session_total/$model->processes)*100) ?>%</td>
                    <td>连接池使用率 参考值：60%</td>
                </tr>
                <tr>
                    <td>总计会话数</td>
                    <td><?= $model->session_total.' (最大允许:'.$model->processes.')'; ?></td>
                    <td>当前数据库的会话总数</td>
                </tr>
                <tr>
                    <td>活动会话数</td>
                    <td><?= $model->session_actives; ?></td>
                    <td>当前数据库正在运行的SQL会话数 参考值:<=30</td>
                </tr>
                <tr>
                    <td>等待会话数</td>
                    <td><?= $model->session_waits; ?></td>
                    <td>进程中SQL会话等待数 参考值：<=5</td>
                </tr>
                <tr>
                    <td>新增硬解析数</td>
                    <td><?= $model->parse_count_hard_inc; ?></td>
                    <td>当前新增硬解析数</td>
                </tr>
                <tr>
                    <td>无效对象数</td>
                    <td><?= $model->objects_invalid_count; ?></td>
                    <td>无效的约束、触发器、函数等</td>
                </tr>
                <tr>
                    <td>无效索引数</td>
                    <td><?= $model->indexes_invalid_count; ?></td>
                    <td>无效索引/blevel>3的索引</td>
                </tr>
                <tr>
                    <td>SGA命中率</td>
                    <td><?= $model->buffer_pool_rate; ?></td>
                    <td>SGA数据缓存命中率</td>
                </tr>
                <tr>
                    <td>数据字典命中率</td>
                    <td><?= $model->data_dict_rate; ?></td>
                    <td>数据字典命中率</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="col-xs-4 text-center ">
            <div id="main_chart1" class="line-chart-x4 mt5"></div>
            <div id="main_chart2" class="line-chart-x4 mt5"></div>
        </div>
    </div>


    <?php \common\widgets\JsBlock::begin() ?>
    <script type="text/javascript">

        var myChart = echarts.init(document.getElementById('main_chart1'));

        myChart.setOption({
            //backgroundColor: '#FFF',
            color:['#FF9966','#FF0033'],
            title : {
                text: '',
                subtext: '',
                x: 'center',
                textStyle: {
                    fontSize: 14,
                    fontWeight: 'bolder',
                    color: '#FFFFFF'
                }
            },
            legend: {
                data:['Active Sessions','Waits Sessions'],
                x: 'left',
                textStyle: {
                    fontSize: 8,
                    color: '#333333'
                }
            },
            grid: {
                x: '40px',
                x2: '20px',
                y: '30px',
                y2: '30px'
            },
            tooltip : {
                trigger: 'axis'
            },

            toolbox: {
                show : false,
                feature : {
                    mark : {show: true},
                    dataView : {show: true, readOnly: false},
                    magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    axisLine: {onZero: false},
                    data : [
                        <?php
                            $time_array=array();
                            foreach($chart_data as $item):
                                $date = date('m/d',strtotime($item['create_time']));
                                $time = date('H:i',strtotime($item['create_time']));
                                $time_array[]="'$time'";
                            endforeach;
                            echo implode(',',$time_array);
                         ?>
                    ]
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:'Active Sessions',
                    type:'line',
                    smooth:true,
                    //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data:[
                        <?php
                            $data_array=array();
                            foreach($chart_data as $item):
                                $data_array[] = $item['session_actives'];
                            endforeach;
                            echo implode(',',$data_array);
                         ?>
                    ]
                },
                {
                    name:'Waits Sessions',
                    type:'line',
                    smooth:true,
                    //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data:[
                        <?php
                            $data_array=array();
                            foreach($chart_data as $item):
                                $data_array[] = $item['session_waits'];
                            endforeach;
                            echo implode(',',$data_array);
                         ?>
                    ]
                },

            ]
        });

    </script>

    <script type="text/javascript">

        var myChart = echarts.init(document.getElementById('main_chart2'));
        myChart.setOption({
            //backgroundColor: '#FFF',
            color:['#FF9966','#FF0033'],
            title : {
                text: '',
                subtext: '',
                x: 'center',
                textStyle: {
                    fontSize: 14,
                    fontWeight: 'bolder',
                    color: '#FFFFFF'
                }
            },
            legend: {
                data:['SQLMonitor Lag','Temp SQL'],
                x: 'left',
                textStyle: {
                    fontSize: 8,
                    color: '#333333'
                }
            },
            grid: {
                x: '40px',
                x2: '20px',
                y: '30px',
                y2: '30px'
            },
            tooltip : {
                trigger: 'axis'
            },

            toolbox: {
                show : false,
                feature : {
                    mark : {show: true},
                    dataView : {show: true, readOnly: false},
                    magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    axisLine: {onZero: false},
                    data : [
                        <?php
                            $time_array=array();
                            foreach($chart_data as $item):
                                $date = date('m/d',strtotime($item['create_time']));
                                $time = date('H:i',strtotime($item['create_time']));
                                $time_array[]="'$time'";
                            endforeach;
                            echo implode(',',$time_array);
                         ?>
                    ]
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:'SQLMonitor Lag',
                    type:'line',
                    smooth:true,
                    //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data:[
                        <?php
                            $data_array=array();
                            foreach($chart_data as $item):
                                $data_array[] = $item['sqlmonitor_count'];
                            endforeach;
                            echo implode(',',$data_array);
                         ?>
                    ]
                },
                {
                    name:'Temp SQL',
                    type:'line',
                    smooth:true,
                    //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data:[
                        <?php
                            $data_array=array();
                            foreach($chart_data as $item):
                                $data_array[] = $item['temp_tbs_sql_count'];
                            endforeach;
                            echo implode(',',$data_array);
                         ?>
                    ]
                },

            ]
        });

    </script>


    <?php \common\widgets\JsBlock::end()?>


</div>
