<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\oracle\models\OracleStatus */

$this->title = 'Create Oracle Status';
$this->params['breadcrumbs'][] = ['label' => 'Oracle Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oracle-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
