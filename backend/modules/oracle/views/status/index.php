<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\helpers\Tools;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\oracle\models\OracleStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Oracle 性能监控平台';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oracle-status-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'host',
            'port',
            'tags',
            [
                'attribute' => 'connect',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return $model->connect == 1 ? "<span class='btn btn-xs bg-green btn-flat'>正常</span>" : "<span class='btn btn-xs bg-red btn-flat'>异常</span>";
                    },

                'headerOptions' => ['width' => '75'],

            ],
            [
                'attribute' => 'open_mode',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return $model->open_mode == 'READ WRITE' ? "<span class='btn btn-xs bg-green btn-flat'>读/写</span>" : "<span class='btn btn-xs bg-orange btn-flat'>只读</span>";
                    },

                'headerOptions' => ['width' => '75'],

            ],
            [
                'attribute' => 'uptime',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return Tools::checkUptime(time()-strtotime($model->uptime));
                    },
                'headerOptions' => ['width' => '75'],

            ],
            'version',
            'instance_name',
             'instance_status',
             'database_role',
             'archiver',
             'session_total',
             'session_actives',
             'session_waits',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Operate'),
                'headerOptions' => ['width' => '75'],
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',  Yii::$app->urlManager->createUrl(['oracle/status/view','server'=>$model->host.'-'.$model->port]), ['title' => '查看监控详情' ]);
                    },

                ],

            ],
        ],
    ]); ?>

</div>
