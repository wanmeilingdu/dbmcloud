<?php

namespace backend\modules\oracle\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\oracle\models\OracleStatus;

/**
 * OracleStatusSearch represents the model behind the search form about `backend\modules\oracle\models\OracleStatus`.
 */
class OracleStatusSearch extends OracleStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'connect', 'session_total', 'session_actives', 'session_waits', 'dg_delay', 'processes', 'session_logical_reads_persecond', 'physical_reads_persecond', 'physical_writes_persecond', 'physical_read_io_requests_persecond', 'physical_write_io_requests_persecond', 'db_block_changes_persecond', 'os_cpu_wait_time', 'logons_persecond', 'logons_current', 'opened_cursors_persecond', 'opened_cursors_current', 'user_commits_persecond', 'user_rollbacks_persecond', 'user_calls_persecond', 'db_block_gets_persecond', 'created_at', 'updated_at'], 'integer'],
            [['host', 'port', 'tags', 'instance_name', 'instance_role', 'instance_status', 'database_role', 'open_mode', 'protection_mode', 'host_name', 'database_status', 'startup_time', 'uptime', 'version', 'archiver', 'dg_stats', 'create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OracleStatus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'connect' => $this->connect,
            'session_total' => $this->session_total,
            'session_actives' => $this->session_actives,
            'session_waits' => $this->session_waits,
            'dg_delay' => $this->dg_delay,
            'processes' => $this->processes,
            'session_logical_reads_persecond' => $this->session_logical_reads_persecond,
            'physical_reads_persecond' => $this->physical_reads_persecond,
            'physical_writes_persecond' => $this->physical_writes_persecond,
            'physical_read_io_requests_persecond' => $this->physical_read_io_requests_persecond,
            'physical_write_io_requests_persecond' => $this->physical_write_io_requests_persecond,
            'db_block_changes_persecond' => $this->db_block_changes_persecond,
            'os_cpu_wait_time' => $this->os_cpu_wait_time,
            'logons_persecond' => $this->logons_persecond,
            'logons_current' => $this->logons_current,
            'opened_cursors_persecond' => $this->opened_cursors_persecond,
            'opened_cursors_current' => $this->opened_cursors_current,
            'user_commits_persecond' => $this->user_commits_persecond,
            'user_rollbacks_persecond' => $this->user_rollbacks_persecond,
            'user_calls_persecond' => $this->user_calls_persecond,
            'db_block_gets_persecond' => $this->db_block_gets_persecond,
            'create_time' => $this->create_time,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'port', $this->port])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'instance_name', $this->instance_name])
            ->andFilterWhere(['like', 'instance_role', $this->instance_role])
            ->andFilterWhere(['like', 'instance_status', $this->instance_status])
            ->andFilterWhere(['like', 'database_role', $this->database_role])
            ->andFilterWhere(['like', 'open_mode', $this->open_mode])
            ->andFilterWhere(['like', 'protection_mode', $this->protection_mode])
            ->andFilterWhere(['like', 'host_name', $this->host_name])
            ->andFilterWhere(['like', 'database_status', $this->database_status])
            ->andFilterWhere(['like', 'startup_time', $this->startup_time])
            ->andFilterWhere(['like', 'uptime', $this->uptime])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'archiver', $this->archiver])
            ->andFilterWhere(['like', 'dg_stats', $this->dg_stats]);

        return $dataProvider;
    }
}
