<?php

namespace backend\modules\mysql\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "servers_mysql".
 *
 * @property integer $id
 * @property string $host
 * @property string $port
 * @property string $username
 * @property string $password
 * @property string $tags
 * @property integer $monitor
 * @property integer $send_mail
 * @property string $send_mail_to_list
 * @property integer $send_sms
 * @property string $send_sms_to_list
 * @property string $send_slowquery_to_list
 * @property integer $alarm_threads_connected
 * @property integer $alarm_threads_running
 * @property integer $alarm_threads_waits
 * @property integer $alarm_repl_status
 * @property integer $alarm_repl_delay
 * @property integer $threshold_warning_threads_connected
 * @property integer $threshold_warning_threads_running
 * @property integer $threshold_warning_threads_waits
 * @property integer $threshold_warning_repl_delay
 * @property integer $threshold_critical_threads_connected
 * @property integer $threshold_critical_threads_running
 * @property integer $threshold_critical_threads_waits
 * @property integer $threshold_critical_repl_delay
 * @property integer $slow_query
 * @property integer $binlog_auto_purge
 * @property integer $binlog_store_days
 * @property integer $bigtable_monitor
 * @property integer $bigtable_size
 * @property integer $is_delete
 * @property integer $user_id
 * @property string $create_at
 *  * @property string $update_at
 */
class ServersMysql extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servers_mysql';
    }

    /**
     * create_time, update_time to now()
     * crate_user_id, update_user_id to current login user id
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            // BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['host', 'port'], 'required'],
            [['monitor', 'send_mail', 'send_sms', 'alarm_threads_connected', 'alarm_threads_running', 'alarm_threads_waits', 'alarm_repl_status', 'alarm_repl_delay', 'threshold_warning_threads_connected', 'threshold_warning_threads_running', 'threshold_warning_threads_waits', 'threshold_warning_repl_delay', 'threshold_critical_threads_connected', 'threshold_critical_threads_running', 'threshold_critical_threads_waits', 'threshold_critical_repl_delay', 'slow_query', 'binlog_auto_purge', 'binlog_store_days', 'bigtable_monitor', 'bigtable_size', 'is_delete', 'user_id','created_at','updated_at'], 'integer'],
            [['host', 'username', 'password'], 'string', 'max' => 30],
            [['port'], 'string', 'max' => 10],
            [['tags'], 'string', 'max' => 50],
            [['send_mail_to_list', 'send_sms_to_list', 'send_slowquery_to_list'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => '主机',
            'port' => '端口',
            'username' => '账号',
            'password' => '密码',
            'tags' => '标签',
            'monitor' => '开启监控',
            'send_mail' => '邮件通知',
            'send_mail_to_list' => '告警邮件接收人',
            'send_sms' => '发送短信',
            'send_sms_to_list' => '告警短信接收人',
            'send_slowquery_to_list' => '慢查询推送',
            'alarm_threads_connected' => '连接线程数',
            'alarm_threads_running' => '运行进程数',
            'alarm_threads_waits' => '进程等待数',
            'alarm_repl_status' => '复制状态',
            'alarm_repl_delay' => '复制延时',
            'threshold_warning_threads_connected' => '连接线程数警告阀值',
            'threshold_warning_threads_running' => '运行进程数警告阀值',
            'threshold_warning_threads_waits' => '进程等待数警告阀值',
            'threshold_warning_repl_delay' => '备库延时告警阀值',
            'threshold_critical_threads_connected' => '连接线程数紧急阀值',
            'threshold_critical_threads_running' => '运行进程数紧急阀值',
            'threshold_critical_threads_waits' => '进程等待数紧急阀值',
            'threshold_critical_repl_delay' => '复制延时紧急阀值',
            'slow_query' => '慢查询分析',
            'binlog_auto_purge' => 'Binlog自动清理',
            'binlog_store_days' => 'Binlog保留天数',
            'bigtable_monitor' => '大表监控',
            'bigtable_size' => '大表采集阀值',
            'is_delete' => '已删除',
            'user_id' => '用户ID',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',
        ];
    }


    /**
     * Before save.
     *
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            $this->user_id = Yii::$app->user->id;
            return true;
        }
        else
            return false;
    }

    /**
     * After save.
     *
     */
    /*public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add your code here
    }*/

}
