<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\mysql\models\MysqlStatus */

$this->title = $model->host . ':' . $model->port . ' [' . $model->tags .':'.$model->role. ']';
$this->params['breadcrumbs'][] = ['label' => 'MySQL 性能监控平台', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->host.':'.$model->port;

?>

<?php
$data = $model->find()->orderBy(['id' => SORT_DESC, 'host' => SORT_ASC])->all();
echo $this->render('_tab',['model'=>$model]);
?>

<div class="container-fluid mt15">
    <?php if(($model->threads_connected/$model->max_connections*100) >= 75): ?>
    <div class="alert alert-danger  alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-warning"></i> 警告!</h4>
        数据库连接池使用率过高，请增加max_connections参数来调整连接数限制，以免造成连接异常.
    </div>
    <?php elseif(($model->open_files/$model->open_files_limit*100) >= 60): ?>
        <div class="alert alert-danger  alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-warning"></i> 警告!</h4>
            打开文件句柄使用率过高，请增加open_file_limits参数来调整限制，以免造成数据库异常.
        </div>
    <?php else: ?>
        <div class="alert alert-success  alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> 正常!</h4>
            数据库连接资源和文件资源分配充足.
        </div>
    <?php endif; ?>
    <div id="main_chart" class="col-xs-6 text-center pie_chart"></div>
    <div id="main_chart2" class="col-xs-6 text-center pie_chart"></div>
</div>



<?php \common\widgets\JsBlock::begin() ?>

<script type="text/javascript">

    var myChart = echarts.init(document.getElementById('main_chart'));

    myChart.setOption({
        //backgroundColor: '#FFFFFF',
        color:['#DD4B39','#00A65A'],
        title : {
            text: '连接池使用率',
            subtext: '连接使用率超过75%请增加',
            x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient : 'vertical',
            x : 'left',
            data:['已用连接','可用连接']
        },
        toolbox: {
            show : true,
            feature : {
                //mark : {show: true},
                dataView : {show: true, readOnly: false},
                magicType : {
                    show: true,
                    type: ['pie', 'funnel'],
                    option: {
                        funnel: {
                            x: '25%',
                            width: '50%',
                            funnelAlign: 'left',
                            max: 1548
                        }
                    }
                },
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        series : [
            {
                name:'连接数使用',
                type:'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:<?=$model->threads_connected; ?>, name:'已用连接'},
                    {value:<?=($model->max_connections-$model->threads_connected); ?>, name:'可用连接'},
                ]
            }
        ]
    });

</script>

<script type="text/javascript">

    var myChart = echarts.init(document.getElementById('main_chart2'));

    myChart.setOption({
        color:['#DD4B39','#00A65A'],
        title : {
            text: '文件句柄资源',
            subtext: '文件句柄使用率超过60%请增加',
            x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient : 'vertical',
            x : 'left',
            data:['已用文件句柄','可用文件句柄']
        },
        toolbox: {
            show : true,
            feature : {
                //mark : {show: true},
                dataView : {show: true, readOnly: false},
                magicType : {
                    show: true,
                    type: ['pie', 'funnel'],
                    option: {
                        funnel: {
                            x: '25%',
                            width: '50%',
                            funnelAlign: 'left',
                            max: 1548
                        }
                    }
                },
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        series : [
            {
                name:'文件句柄',
                type:'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:<?=$model->open_files; ?>, name:'已用文件句柄'},
                    {value:<?=($model->open_files_limit-$model->open_files); ?>, name:'可用文件句柄'},
                ]
            }
        ]
    });

</script>
<?php \common\widgets\JsBlock::end()?>


