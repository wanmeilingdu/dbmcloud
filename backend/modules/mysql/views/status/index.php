<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\helpers\Tools;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MysqlStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'MySQL 性能监控平台';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mysql-status-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'tags',
            'host',
            'port',

            [
                'attribute' => 'connect',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return $model->connect == 1 ? "<span class='btn btn-xs bg-green btn-flat'>正常</span>" : "<span class='btn btn-xs bg-red btn-flat'>异常</span>";
                    },

                'headerOptions' => ['width' => '75'],

            ],
            [
                'attribute' => 'role',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return Tools::checkValue($model->role);
                    },

                'headerOptions' => ['width' => '75'],

            ],
            [
                'attribute' => 'uptime',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return Tools::checkUptime($model->uptime);
                    },
                'headerOptions' => ['width' => '75'],

            ],
            [
                'attribute' => 'version',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return Tools::checkValue($model->version);
                    },
                'headerOptions' => ['width' => '90'],

            ],

            [
                'attribute' => 'threads_connected',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return Tools::checkValue($model->threads_connected);
                    },
                'headerOptions' => ['width' => '75'],

            ],

            [
                'attribute' => 'threads_running',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return Tools::checkValue($model->threads_running);
                    },
                'headerOptions' => ['width' => '75'],

            ],

            [
                'attribute' => 'threads_waits',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return Tools::checkValue($model->threads_waits);
                    },
                'headerOptions' => ['width' => '75'],

            ],
            // 'threads_created',
            // 'threads_cached',
            // 'connections',
            // 'aborted_clients',
            // 'aborted_connects',
            // 'connections_persecond',
            // 'bytes_received_persecond',
            // 'bytes_sent_persecond',
            // 'com_select_persecond',
            // 'com_insert_persecond',
            // 'com_update_persecond',
            // 'com_delete_persecond',
            // 'com_commit_persecond',
            // 'com_rollback_persecond',
            // 'questions_persecond',

            [
                'attribute' => 'queries_persecond',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return Tools::checkValue($model->queries_persecond);
                    },
                'headerOptions' => ['width' => '75'],

            ],

            [
                'attribute' => 'transaction_persecond',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return Tools::checkValue($model->transaction_persecond);
                    },
                'headerOptions' => ['width' => '75'],

            ],
            // 'created_tmp_tables_persecond',
            // 'created_tmp_disk_tables_persecond',
            // 'created_tmp_files_persecond',
            // 'table_locks_immediate_persecond',
            // 'table_locks_waited_persecond',
            // 'key_buffer_size',
            // 'sort_buffer_size',
            // 'join_buffer_size',
            // 'key_blocks_not_flushed',
            // 'key_blocks_unused',
            // 'key_blocks_used',
            // 'key_read_requests_persecond',
            // 'key_reads_persecond',
            // 'key_write_requests_persecond',
            // 'key_writes_persecond',
            // 'innodb_version',
            // 'innodb_buffer_pool_instances',
            // 'innodb_buffer_pool_size',
            // 'innodb_doublewrite',
            // 'innodb_file_per_table',
            // 'innodb_flush_log_at_trx_commit',
            // 'innodb_flush_method',
            // 'innodb_force_recovery',
            // 'innodb_io_capacity',
            // 'innodb_read_io_threads',
            // 'innodb_write_io_threads',
            // 'innodb_buffer_pool_pages_total',
            // 'innodb_buffer_pool_pages_data',
            // 'innodb_buffer_pool_pages_dirty',
            // 'innodb_buffer_pool_pages_flushed',
            // 'innodb_buffer_pool_pages_free',
            // 'innodb_buffer_pool_pages_misc',
            // 'innodb_page_size',
            // 'innodb_pages_created',
            // 'innodb_pages_read',
            // 'innodb_pages_written',
            // 'innodb_row_lock_current_waits',
            // 'innodb_buffer_pool_pages_flushed_persecond',
            // 'innodb_buffer_pool_read_requests_persecond',
            // 'innodb_buffer_pool_reads_persecond',
            // 'innodb_buffer_pool_write_requests_persecond',
            // 'innodb_rows_read_persecond',
            // 'innodb_rows_inserted_persecond',
            // 'innodb_rows_updated_persecond',
            // 'innodb_rows_deleted_persecond',
            // 'query_cache_hitrate',
            // 'thread_cache_hitrate',
            // 'key_buffer_read_rate',
            // 'key_buffer_write_rate',
            // 'key_blocks_used_rate',
            // 'created_tmp_disk_tables_rate',
            // 'connections_usage_rate',
            // 'open_files_usage_rate',
            // 'open_tables_usage_rate',
            // 'create_time',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Operate'),
                'headerOptions' => ['width' => '75'],
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',  Yii::$app->urlManager->createUrl(['mysql/status/view','server'=>$model->host.'-'.$model->port]), ['title' => '查看监控详情' ]);
                    },

                ],

            ],
        ],
    ]); ?>

</div>
