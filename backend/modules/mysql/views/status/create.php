<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MysqlStatus */

$this->title = 'Create ' . 'Mysql Status';
$this->params['breadcrumbs'][] = ['label' => 'Mysql Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mysql-status-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
