<?php

namespace backend\modules\config\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\config\models\MysqlServer;

/**
 * MysqlServerSearch represents the model behind the search form about `backend\modules\config\models\MysqlServer`.
 */
class MysqlServerSearch extends MysqlServer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'monitor', 'send_mail', 'send_sms', 'alarm_threads_connected', 'alarm_threads_running', 'alarm_threads_waits', 'alarm_repl_status', 'alarm_repl_delay', 'threshold_threads_connected', 'threshold_threads_running', 'threshold_threads_waits', 'threshold_repl_delay', 'slow_query', 'bigtable_monitor', 'bigtable_size', 'created_at', 'updated_at'], 'integer'],
            [['host', 'port', 'username', 'password', 'tags', 'send_mail_to_list', 'send_sms_to_list', 'send_slowquery_to_list'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MysqlServer::find();

        $query->orderBy(['tags' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->load($params) && !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'monitor' => $this->monitor,
            'send_mail' => $this->send_mail,
            'send_sms' => $this->send_sms,
            'alarm_threads_connected' => $this->alarm_threads_connected,
            'alarm_threads_running' => $this->alarm_threads_running,
            'alarm_threads_waits' => $this->alarm_threads_waits,
            'alarm_repl_status' => $this->alarm_repl_status,
            'alarm_repl_delay' => $this->alarm_repl_delay,
            'threshold_threads_connected' => $this->threshold_threads_connected,
            'threshold_threads_running' => $this->threshold_threads_running,
            'threshold_threads_waits' => $this->threshold_threads_waits,
            'threshold_repl_delay' => $this->threshold_repl_delay,
            'slow_query' => $this->slow_query,
            'bigtable_monitor' => $this->bigtable_monitor,
            'bigtable_size' => $this->bigtable_size,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'port', $this->port])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'send_mail_to_list', $this->send_mail_to_list])
            ->andFilterWhere(['like', 'send_sms_to_list', $this->send_sms_to_list])
            ->andFilterWhere(['like', 'send_slowquery_to_list', $this->send_slowquery_to_list]);

        return $dataProvider;
    }
}
