<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\MysqlServer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mysql Servers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mysql-server-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'host',
            'port',
            'username',
            'password',
            'tags',
            'monitor',
            'send_mail',
            'send_mail_to_list',
            'send_sms',
            'send_sms_to_list',
            'send_slowquery_to_list',
            'alarm_threads_connected',
            'alarm_threads_running',
            'alarm_threads_waits',
            'alarm_repl_status',
            'alarm_repl_delay',
            'threshold_threads_connected',
            'threshold_threads_running',
            'threshold_threads_waits',
            'threshold_repl_delay',
            'slow_query',
            'bigtable_monitor',
            'bigtable_size',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
