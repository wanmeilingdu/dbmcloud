<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\OracleServer */

$this->title = '新增Oracle实例';
$this->params['breadcrumbs'][] = ['label' => 'Oracle实例', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oracle-server-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
