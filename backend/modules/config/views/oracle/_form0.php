<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\OracleServer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="oracle-server-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'host')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'port')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dsn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tags')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'monitor')->textInput() ?>

    <?= $form->field($model, 'send_mail')->textInput() ?>

    <?= $form->field($model, 'send_mail_to_list')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'send_sms')->textInput() ?>

    <?= $form->field($model, 'send_sms_to_list')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alarm_session_total')->textInput() ?>

    <?= $form->field($model, 'alarm_session_actives')->textInput() ?>

    <?= $form->field($model, 'alarm_session_waits')->textInput() ?>

    <?= $form->field($model, 'alarm_tablespace')->textInput() ?>

    <?= $form->field($model, 'threshold_session_total')->textInput() ?>

    <?= $form->field($model, 'threshold_session_actives')->textInput() ?>

    <?= $form->field($model, 'threshold_session_waits')->textInput() ?>

    <?= $form->field($model, 'threshold_tablespace')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
