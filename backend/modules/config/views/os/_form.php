<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\OsServer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="os-server-form">

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">连接信息</div>
                <div class="panel-body">
                    <?= $form->field($model, 'tags')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'host')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'community')->textInput(['maxlength' => true]) ?>


                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">告警通知</div>
                <div class="panel-body">

                    <?= $form->field($model, 'monitor')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'send_mail')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'send_sms')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'send_mail_to_list')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'send_sms_to_list')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">告警项目</div>
                <div class="panel-body">

                    <?= $form->field($model, 'alarm_os_process')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_os_load')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_os_cpu')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_os_network')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_os_disk')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_os_memory')->dropDownList((\common\models\Base::getOnStatus())) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">告警阀值</div>
                <div class="panel-body">

                    <?= $form->field($model, 'threshold_os_process')->textInput() ?>

                    <?= $form->field($model, 'threshold_os_load')->textInput() ?>

                    <?= $form->field($model, 'threshold_os_cpu')->textInput() ?>

                    <?= $form->field($model, 'threshold_os_network')->textInput() ?>

                    <?= $form->field($model, 'threshold_os_disk')->textInput() ?>

                    <?= $form->field($model, 'threshold_os_memory')->textInput() ?>

                    <?= $form->field($model, 'filter_os_disk')->textInput() ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? '创建' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        <?= Html::a('返回', ['index'], ['class' => 'btn btn-warning']) ?>
                    </div>

                </div>

            </div>

        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
