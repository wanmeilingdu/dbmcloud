<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\MongodbServer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mongodb Servers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mongodb-server-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'host',
            'port',
            'username',
            'password',
            'tags',
            'monitor',
            'send_mail',
            'send_mail_to_list',
            'send_sms',
            'send_sms_to_list',
            'alarm_connections_current',
            'alarm_active_clients',
            'alarm_current_queue',
            'threshold_connections_current',
            'threshold_active_clients',
            'threshold_current_queue',
            'create_time',
        ],
    ]) ?>

</div>
