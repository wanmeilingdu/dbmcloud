<?php

namespace backend\modules\hbase\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->redirect(['rs-status/index']);
        //return $this->render('index');
    }
}
