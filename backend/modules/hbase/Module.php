<?php

namespace backend\modules\hbase;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\hbase\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
