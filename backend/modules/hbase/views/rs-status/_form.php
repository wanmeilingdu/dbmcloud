<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\hbase\models\HbaseRsStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hbase-rs-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'host')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'port')->textInput() ?>

    <?= $form->field($model, 'tags')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'connect')->textInput() ?>

    <?= $form->field($model, 'readRequestCount_sub')->textInput() ?>

    <?= $form->field($model, 'writeRequestCount_sub')->textInput() ?>

    <?= $form->field($model, 'compactionQueueLength')->textInput() ?>

    <?= $form->field($model, 'flushQueueLength')->textInput() ?>

    <?= $form->field($model, 'regionCount')->textInput() ?>

    <?= $form->field($model, 'storeCount')->textInput() ?>

    <?= $form->field($model, 'storeFileCount')->textInput() ?>

    <?= $form->field($model, 'totalRequestCount_sub')->textInput() ?>

    <?= $form->field($model, 'updatesBlockedTime')->textInput() ?>

    <?= $form->field($model, 'totalRequestCount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'GcCount')->textInput() ?>

    <?= $form->field($model, 'GcTimeMillis')->textInput() ?>

    <?= $form->field($model, 'GcTimeMillisConcurrentMarkSweep')->textInput() ?>

    <?= $form->field($model, 'GcCountConcurrentMarkSweep')->textInput() ?>

    <?= $form->field($model, 'queueSize')->textInput() ?>

    <?= $form->field($model, 'QueueCallTime_num_ops_sub')->textInput() ?>

    <?= $form->field($model, 'numCallsInGeneralQueue')->textInput() ?>

    <?= $form->field($model, 'memStoreSize')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'blockCacheSize')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MemHeapUsedM')->textInput() ?>

    <?= $form->field($model, 'createtime')->textInput() ?>

    <?= $form->field($model, 'readRequestCount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'writeRequestCount')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
