<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\hbase\models\HbaseRsStatusSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hbase-rs-status-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'host') ?>

    <?= $form->field($model, 'port') ?>

    <?= $form->field($model, 'tags') ?>

    <?= $form->field($model, 'connect') ?>

    <?php // echo $form->field($model, 'readRequestCount_sub') ?>

    <?php // echo $form->field($model, 'writeRequestCount_sub') ?>

    <?php // echo $form->field($model, 'compactionQueueLength') ?>

    <?php // echo $form->field($model, 'flushQueueLength') ?>

    <?php // echo $form->field($model, 'regionCount') ?>

    <?php // echo $form->field($model, 'storeCount') ?>

    <?php // echo $form->field($model, 'storeFileCount') ?>

    <?php // echo $form->field($model, 'totalRequestCount_sub') ?>

    <?php // echo $form->field($model, 'updatesBlockedTime') ?>

    <?php // echo $form->field($model, 'totalRequestCount') ?>

    <?php // echo $form->field($model, 'GcCount') ?>

    <?php // echo $form->field($model, 'GcTimeMillis') ?>

    <?php // echo $form->field($model, 'GcTimeMillisConcurrentMarkSweep') ?>

    <?php // echo $form->field($model, 'GcCountConcurrentMarkSweep') ?>

    <?php // echo $form->field($model, 'queueSize') ?>

    <?php // echo $form->field($model, 'QueueCallTime_num_ops_sub') ?>

    <?php // echo $form->field($model, 'numCallsInGeneralQueue') ?>

    <?php // echo $form->field($model, 'memStoreSize') ?>

    <?php // echo $form->field($model, 'blockCacheSize') ?>

    <?php // echo $form->field($model, 'MemHeapUsedM') ?>

    <?php // echo $form->field($model, 'createtime') ?>

    <?php // echo $form->field($model, 'readRequestCount') ?>

    <?php // echo $form->field($model, 'writeRequestCount') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
