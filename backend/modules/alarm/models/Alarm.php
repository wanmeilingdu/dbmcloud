<?php

namespace backend\modules\alarm\models;

use Yii;

/**
 * This is the model class for table "alarm".
 *
 * @property string $id
 * @property string $tags
 * @property string $host
 * @property string $port
 * @property string $create_time
 * @property string $db_type
 * @property string $alarm_item
 * @property string $alarm_value
 * @property string $level
 * @property string $message
 * @property string $detail
 * @property integer $send_mail
 * @property integer $send_sms
 */
class Alarm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alarm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_time'], 'safe'],
            [['detail'], 'string'],
            [['send_mail', 'send_sms'], 'integer'],
            [['tags', 'alarm_item', 'alarm_value', 'level'], 'string', 'max' => 50],
            [['host', 'db_type'], 'string', 'max' => 30],
            [['port'], 'string', 'max' => 10],
            [['message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tags' => '标签',
            'host' => '主机',
            'port' => '端口',
            'create_time' => '发送时间',
            'db_type' => 'DB类型',
            'alarm_item' => '告警项目',
            'alarm_value' => '告警值',
            'level' => '级别',
            'message' => '信息',
            'detail' => '异常详情',
            'send_mail' => '邮件',
            'send_sms' => '短信',
        ];
    }

    public static function findNewAlarm(){
        $time_before = date('Y-m-d H:i:s',time()-3600*24);
        return static::find()->where("create_time >= '$time_before'")->orderBy('create_time DESC')->limit(10)->asArray()->all();
    }
}
