<?php

namespace backend\modules\alarm\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\alarm\models\Alarm;

/**
 * AlarmSearch represents the model behind the search form about `backend\modules\alarm\models\Alarm`.
 */
class AlarmSearch extends Alarm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'send_mail', 'send_sms'], 'integer'],
            [['tags', 'host', 'port', 'create_time', 'db_type', 'alarm_item', 'alarm_value', 'level', 'message', 'detail'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Alarm::find();

        $query->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->load($params) && !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'create_time' => $this->create_time,
            'send_mail' => $this->send_mail,
            'send_sms' => $this->send_sms,
        ]);

        $query->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'port', $this->port])
            ->andFilterWhere(['like', 'db_type', $this->db_type])
            ->andFilterWhere(['like', 'alarm_item', $this->alarm_item])
            ->andFilterWhere(['like', 'alarm_value', $this->alarm_value])
            ->andFilterWhere(['like', 'level', $this->level])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'detail', $this->detail]);

        return $dataProvider;
    }
}
