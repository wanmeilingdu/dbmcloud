<?php

namespace backend\modules\alarm;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\alarm\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
