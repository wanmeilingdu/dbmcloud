<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\alarm\models\Alarm */

$this->title = $model->host . ':' . $model->port . ' [' . $model->tags . ']';
$this->params['breadcrumbs'][] = ['label' => '告警列表', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alarm-view">


    <p>
        <?php //echo Html::button(Yii::t('app', 'Print'), ['class' => 'btn btn-info','onclick'=>'preview()']) ?>
        <?= Html::a(Yii::t('app', 'Return'), ['index'], ['class' => 'btn btn-warning']) ?>
    </p>

    <!--startprint-->

    <h4>告警详细信息</h4>
    <div>
        <table class="table table-bordered table-striped">
            <tbody>
            <tr>
                <td>主机信息</td>
                <td><?=$model->db_type.'-'.$model->host.':'.$model->port.'/'.$model->tags; ?></td>
                <td>发生时间</td>
                <td><?= $model->create_time; ?></td>
            </tr>
            <tr>
                <td>告警信息</td>
                <td><?= $model->message; ?></td>
                <td>告警级别</td>
                <td><?= $model->level; ?></td>
            </tr>

            <tr>
                <td colspan="4">详细异常信息:</td>
            </tr>
            <tr>
                <td colspan="4">
                    <iframe id="detail" src="<?php echo  Yii::$app->urlManager->createUrl(['alarm/default/detail','id'=>$model->id])?>" width="880" height="320" frameborder="0" scrolling="auto" style="border: 1px solid gray"></iframe></td>
            </tr>
            </tbody>
        </table>
    </div>



    <!--endprint-->


</div>


