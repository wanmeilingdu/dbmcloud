<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\alarm\models\AlarmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '告警列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alarm-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'tags',
            'host',
            'port',
            'create_time',
            'db_type',
            'level',
            'message',
            // 'detail:ntext',
            'send_mail',
            'send_sms',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => Yii::t('app', 'Operate'),
                'headerOptions' => ['width' => '65'],
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',  Yii::$app->urlManager->createUrl(['alarm/default/view','id'=>$model['id']]), ['title' => '查看详情' ]);
                    },

                ],

            ],
        ],
    ]); ?>

</div>
