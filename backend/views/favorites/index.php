<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\FavoritesCategory;
use yii\widgets\ActiveForm;
use common\models\Base;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FavoritesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Favorites');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid favorites-index">

    <div class="row">


        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= Yii::t('app', 'Favorites ').Yii::t('app', 'Category') ?>
                </div>
                <div class="panel-body">
                    <?= GridView::widget([
                        'dataProvider' => $categoryProvider,
                        'columns' => [
                            'name',
                            [
                                'label'=>Yii::t('app', 'Operate'),
                                'headerOptions' => ['width'=>'55'],
                                'format'=>'raw',
                                'value' => function($model){
                                    return
                                        Html::a('<span class="glyphicon glyphicon-pencil"></span>',Yii::$app->getUrlManager()->createUrl(['favorites/update-category','uuid'=>$model['uuid']]), ['title' => Yii::t('app','Update') ]).'&nbsp'
                                        .Html::a('<span class="glyphicon glyphicon-trash"></span>',Yii::$app->getUrlManager()->createUrl(['favorites/delete-category','uuid'=>$model['uuid']]), ['title' => Yii::t('app','Delete') , 'data-method' => 'post','data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?')] );
                                }
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?= Yii::t('app', 'Create ').Yii::t('app', 'Category') ?>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(['method' => 'post', 'action' => ['favorites/create-category']]); ?>

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'sort_order')->textInput() ?>

                    <?= $form->field($model, 'status')->dropDownList((Base::getActiveStatus())) ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

        <div class="col-xs-9">
            <p>
                <?= Html::a(Yii::t('app', 'Create ') . Yii::t('app', 'Favorites'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'name',
                    [
                        'attribute' => 'category_id',
                        //'headerOptions' => ['width' => '110'],
                        'value' => function ($model) {
                            return \common\models\FavoritesCategory::getName($model->category_id);
                        },
                        'filter' => Html::activeDropDownList(
                            $searchModel,
                            'category_id',
                            ArrayHelper::map(\common\models\FavoritesCategory::FindTotal(), 'id', 'name'),
                            ['class' => 'form-control select-filter', 'prompt' => Yii::t('app', 'Please Filter')]
                        ),

                    ],
                    //'url:url',
                    [
                        'attribute' => 'url',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a($model->url, $model->url, ['Target' => 'blank']);
                        }
                    ],
                    'description:ntext',

                    [
                        'label'=>Yii::t('app', 'Operate'),
                        'headerOptions' => ['width'=>'85'],
                        'format'=>'raw',
                        'value' => function($model){
                            return
                                Html::a('<span class="glyphicon glyphicon-eye-open"></span>',Yii::$app->getUrlManager()->createUrl(['favorites/view','uuid'=>$model['uuid']]), ['title' => Yii::t('app','View') ]).'&nbsp'
                                .Html::a('<span class="glyphicon glyphicon-pencil"></span>',Yii::$app->getUrlManager()->createUrl(['favorites/update','uuid'=>$model['uuid']]), ['title' => Yii::t('app','Update') ]).'&nbsp'
                                .Html::a('<span class="glyphicon glyphicon-trash"></span>',Yii::$app->getUrlManager()->createUrl(['favorites/delete','uuid'=>$model['uuid']]), ['title' => Yii::t('app','Delete') , 'data-method' => 'post','data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?')] );
                        }
                    ],
                ],
            ]); ?>

        </div>




    </div>
