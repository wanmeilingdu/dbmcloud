<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Favorites */

$this->title = Yii::t('app', 'Create ') . Yii::t('app', 'Favorites');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Favorites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="favorites-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
