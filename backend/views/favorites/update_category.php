<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FavoritesCategory */

$this->title = Yii::t('app', 'Update ') . Yii::t('app', 'Favorites Category') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Favorites Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="favorites-category-update">

    <?= $this->render('_form_category', [
        'model' => $model,
    ]) ?>

</div>
