<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ActionLog */

$this->title = Yii::t('app', 'Update ') . Yii::t('app', 'Action Log') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Action Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="action-log-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
