<?php

namespace backend\controllers;

use Yii;
use common\models\Favorites;
use common\models\FavoritesSearch;
use common\models\FavoritesCategory;
use common\models\FavoritesCategorySearch;
use yii\web\Controller;
use yii\base\UserException;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\components\MyTrait;
use backend\models\ActionLog;


/**
 * FavoritesController implements the CRUD actions for Favorites model.
 */
class FavoritesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }

    /**
     * Lists all Favorites models.
     * @return mixed
     */
    public function actionIndex()
    {
        //if(!Yii::$app->user->can('viewYourAuth')) throw new ForbiddenHttpException(Yii::t('app', 'No Auth'));

        $searchModel = new FavoritesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        //catetory form model
        $model = new FavoritesCategory();
        $model->loadDefaultValues();
        //category list
        $categorySearchModel = new FavoritesCategorySearch();
        $categoryProvider = $categorySearchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'categoryProvider' => $categoryProvider,
        ]);
    }

    /**
     * Displays a single Favorites model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($uuid)
    {
        //if(!Yii::$app->user->can('viewYourAuth')) throw new ForbiddenHttpException(Yii::t('app', 'No Auth'));
        $id = MyTrait::getId('Favorites',$uuid);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Favorites model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //if(!Yii::$app->user->can('createYourAuth')) throw new ForbiddenHttpException(Yii::t('app', 'No Auth'));

        $model = new Favorites();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            ActionLog::log(Yii::t('app', 'Create ').Yii::t('app', 'Favorites').' ID:'.$model->id);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Favorites model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($uuid)
    {
        //if(!Yii::$app->user->can('updateYourAuth')) throw new ForbiddenHttpException(Yii::t('app', 'No Auth'));
        $id = MyTrait::getId('Favorites',$uuid);
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            ActionLog::log(Yii::t('app', 'Update ').Yii::t('app', 'Favorites').' ID:'.$id);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Favorites model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($uuid)
    {
        //if(!Yii::$app->user->can('deleteYourAuth')) throw new ForbiddenHttpException(Yii::t('app', 'No Auth'));
        $id = MyTrait::getId('Favorites',$uuid);
        $this->findModel($id)->delete();
        /*$model = $this->findModel($id);
        $model->status = Status::STATUS_DELETED;
        $model->save();*/
        ActionLog::log(Yii::t('app', 'Delete ').Yii::t('app', 'Favorites').' ID:'.$id);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Favorites model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Favorites the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Favorites::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*
     * FavoritesCateogry
    */

    public function actionCreateCategory()
    {
        //if(!Yii::$app->user->can('createYourAuth')) throw new ForbiddenHttpException(Yii::t('app', 'No Auth'));

        $model = new FavoritesCategory();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            ActionLog::log(Yii::t('app', 'Create ').Yii::t('app', 'Favorites ').Yii::t('app', 'Category').' ID:'.$model->id);
            return $this->redirect(['index']);
        } else {

            print_r($model->errors);exit;
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateCategory($uuid)
    {
        //if(!Yii::$app->user->can('updateYourAuth')) throw new ForbiddenHttpException(Yii::t('app', 'No Auth'));
        $id = MyTrait::getId('FavoritesCategory',$uuid);
        $model = $this->findCategoryModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            ActionLog::log(Yii::t('app', 'Update ').Yii::t('app', 'Favorites ').Yii::t('app', 'Category').' ID:'.$id);
            return $this->redirect(['index']);
        } else {
            return $this->render('update_category', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeleteCategory($uuid)
    {
        //if(!Yii::$app->user->can('deleteYourAuth')) throw new ForbiddenHttpException(Yii::t('app', 'No Auth'));
        $id = MyTrait::getId('FavoritesCategory',$uuid);
        MyTrait::checkUsing('FavoritesCategory',$id);
        $this->findCategoryModel($id)->delete();
        /*$model = $this->findModel($id);
        $model->status = Status::STATUS_DELETED;
        $model->save();*/
        ActionLog::log(Yii::t('app', 'Delete ').Yii::t('app', 'Favorites ').Yii::t('app', 'Category').' ID:'.$id);
        return $this->redirect(['index']);
    }

    protected function findCategoryModel($id)
    {
        if (($model = FavoritesCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
