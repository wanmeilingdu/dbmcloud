<?php

return [
    'Create ' => '创建',
    'Create' => '创建',
    'Update ' => '更新',
    'Update' => '更新',
    'Delete' => '删除',

    'Setting' => '系统设置',

    'info' => '机构信息',
    'basic' => '基本配置',
    'smtp' => '邮件服务器',

    'organizationName' => '机构名称',
    'organizationPhone' => '联系电话',
    'organizationFax' => '传真号码',
    'organizationEmail' => '电子邮件',
    'organizationUrl' => '官方网站',
    'organizationAddress' => '机构地址',
    'organizationTaxNum' => '机构税号',

    'timezone' => '时区',
    'commentCheck' => '评论审核',

    'smtpHost' => '服务器',
    'smtpPort' => '端口',
    'smtpUser' => '用户名',
    'smtpPassword' => '密码',
    'smtpMail' => '显示地址',

];
